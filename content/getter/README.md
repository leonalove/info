﻿###  [GNews](https://gitlab.com/leonalove/info) | [蓋特](https://gitlab.com/leonalove/info/blob/master/content/getter/README.md) | [視頻文字](https://gitlab.com/leonalove/info/blob/master/content/README.md) | [關於](https://gitlab.com/leonalove/home/tree/master/about)
---

推薦文章:

[:statue_of_liberty: 2020年6月4日新中国联邦宣言](https://gitlab.com/leonalove/info/blob/master/content/docs/declaration-of-the-New-Federal-State-of-China/README.md)

[香港抗争者致内地同胞书](https://gitlab.com/leonalove/news/blob/master/2019/08/a_letter_from_the_hong_kong_people.md)

[郭文贵爆料革命 · 启蒙开智入门指南](https://gitlab.com/leonalove/info/issues/1)

[ 蓋特 :fire: :new:](https://gitlab.com/leonalove/info/blob/master/content/getter/README.md) 



[8月29日 新中国联邦日本战友们在大阪的抗议游行活动现场（四）](/content/getter/content/2020/08/5f4a6a5cd3bc84639c7d799f.md)

**郭文贵MILES** `2020-08-29T14:46:52`
##

[8月29日 新中国联邦日本战友们在大阪的抗议游行活动现场（三）](/content/getter/content/2020/08/5f4a5927d3bc84639c7d6593.md)

**郭文贵MILES** `2020-08-29T13:33:27`
##

[8月29日 新中国联邦日本战友们在大阪的抗议游行活动现场（二）](/content/getter/content/2020/08/5f4a4968d3bc84639c7d53ca.md)

**郭文贵MILES** `2020-08-29T12:26:16`
##

[8月29日 新中国联邦日本战友们在大阪的抗议游行活动现场（一）](/content/getter/content/2020/08/5f4a48a2d3bc84639c7d5310.md)

**郭文贵MILES** `2020-08-29T12:22:58`
##

[8月29日 新中国联邦德国战友们在慕尼黑的抗议现场](/content/getter/content/2020/08/5f4a4729d3bc84639c7d5178.md)

**郭文贵MILES** `2020-08-29T12:16:41`
##

[8月29日 尊敬的郝海东先生向全球所有参加游行的战友们致敬！消灭共产党，郝海东先生永远和战友们站在一起！共产党你完蛋了！](/content/getter/content/2020/08/5f4a4255d3bc84639c7d4d29.md)

**郭文贵MILES** `2020-08-29T11:56:05`
##

[8月28日 班农战斗室：比尔格茨披露夏威夷司法文件上未公布的一段细节，这些人有一个20亿美元的阴谋；我们目前首先要做的是把中共国定为敌国，而不是战略竞争对手；郭...](/content/getter/content/2020/08/5f4a0174d3bc84639c7d1559.md)

**郭文贵MILES** `2020-08-29T07:19:16`
##

[8月28日 尊敬的战友们，明天文贵上午直播，请大家去班农战斗室观看，文贵再次荣幸参加战斗室节目。 一切都已经开始！](/content/getter/content/2020/08/5f49ac26d3bc84639c7cc0e4.md)

**郭文贵MILES** `2020-08-29T01:15:18`
##

[8月28日 班农战斗室：比尔格茨指出拜登在大选期间表示会对中共国强硬，但实际上他们代表了三十年来对华那套失败的政策。面对中共国带来安全威胁，他们刚在南海进行了新...](/content/getter/content/2020/08/5f49a9dfd3bc84639c7cbe80.md)

**郭文贵MILES** `2020-08-29T01:05:35`
##

[8月27日 川普总统在共和党大会上发言：我向美国民众保证，我们对中共国采取了迄今为止在美国历史上最强硬，最大胆直接，最强有力地行动！我们要去找中共算账，我们坚决...](/content/getter/content/2020/08/5f49a27ed3bc84639c7cb51e.md)

**郭文贵MILES** `2020-08-29T00:34:06`
##

[8月27日参议院汤姆.科顿在共和党大会上发言：川普总统与共产主义对决，共产中国更是无法比拟，川普总统已经清晰看到中共的威胁，他会让中共为其所为付出代价！  Au...](/content/getter/content/2020/08/5f499e7ed3bc84639c7cb090.md)

**郭文贵MILES** `2020-08-29T00:17:02`
##

[亲爱的战友们，这是VOH欧战团英语组战友们支持慕尼黑游行的动员视频。请战友们欣赏，感谢慕尼黑和欧洲战团VOH战友们对爆料革命和新中国联邦的支持](/content/getter/content/2020/08/5f495219d3bc84639c7c73bd.md)

**郭文贵MILES** `2020-08-28T18:51:05`
##

[8月28日 安倍晋三因病辞任，文贵先生半月前称下任首相会更反共！这是巧合么？  August 28, Shinzo Abe resigned due to th...](/content/getter/content/2020/08/5f492f9ad3bc84639c7c59c1.md)

**郭文贵MILES** `2020-08-28T16:23:54`
##

[8月28日：尊敬的战友们好．这一系列的重大事件．是上天帮助也是我们亿万个灭共同战友之努力的结果，大家从昨天的第一次历史性的共千日党的大会演讲，当中可以判断出多少...](/content/getter/content/2020/08/5f490946d3bc84639c7c337c.md)

**郭文贵MILES** `2020-08-28T13:40:22`
##

[8月27日：这个照片对共产党的本质形容得太好了，请战友们今天要高度关注，今天晚上．共和党大会上几个重要人物的演讲，今天绝对是灭共历史上最关键的时刻之一⚠️⚠️⚠...](/content/getter/content/2020/08/5f484fded3bc84639c7b9780.md)

**郭文贵MILES** `2020-08-28T00:29:18`
##

[8月27日：从这个孙子这篇报导可以．完整地看出来刚刚发生的W S J的几篇报道．和共产党最近的大外宜．欺民贼的黑郭新政策．在海外的执行的证据！由此可见端倪．共产...](/content/getter/content/2020/08/5f47f6a3d3bc84639c7b4b68.md)

**郭文贵MILES** `2020-08-27T18:08:35`
##

[8月27号：尊敬的战友们好．半小时后，9:30我们在澳洲喜马拉雅农场连线中相见吧！https://gtv.org/?videoid=5f47a44dfb4f61...](/content/getter/content/2020/08/5f47aed0d3bc84639c7b07dd.md)

**郭文贵MILES** `2020-08-27T13:02:08`
##

[【重要新闻 News】 1.美国奋斗者组成新共和党  The New Republican Party of American Strivers   https...](/content/getter/content/2020/08/5f4676d7d3bc84639c79ffae.md)

**郭文贵MILES** `2020-08-26T14:51:03`
##

[8月26日：在去战岛的途中……](/content/getter/content/2020/08/5f466c05d3bc84639c79f0b2.md)

**郭文贵MILES** `2020-08-26T14:04:53`
##

[8月25日华尔街日报报道：美方表示，中共国外交官帮助访问美国的军事学者逃避联邦调查局的调查  美国关闭了中共国驻休斯敦领事馆，并下令中共撤除这些研究人员。此前，...](/content/getter/content/2020/08/5f4663b9d3bc84639c79e891.md)

**郭文贵MILES** `2020-08-26T13:29:29`
##

[8月26日CNBC/Change调查发现，在摇摆州，对冠状病毒的担忧下降，川普的支持率上升  根据一项新的CNBC/Change调查显示，2020年六个摇摆州对...](/content/getter/content/2020/08/5f466199d3bc84639c79e68e.md)

**郭文贵MILES** `2020-08-26T13:20:25`
##

[8月25日，国务卿迈克·蓬佩奥在耶路撒冷发表2020年共和党全国代表大会演讲。请阅读他的发言记录。  Sec. of State Mike Pompeo’s 2...](/content/getter/content/2020/08/5f465f15d3bc84639c79e41c.md)

**郭文贵MILES** `2020-08-26T13:09:41`
##

[8月26号：尊敬的战友们好．国务卿在以色列的这个讲话．就是一锤定乾坤……无人再可以改变……有很多有关的重大信息……合适的时候，文贵再向战友们报告，接下来还会有一...](/content/getter/content/2020/08/5f465b0cd3bc84639c79e067.md)

**郭文贵MILES** `2020-08-26T12:52:28`
##

[8月25日：与我们伟大的亲爱的战友们一起度过的美好的时光……我们的GTV……我们真情……我们的梦想……我们的奋斗！](/content/getter/content/2020/08/5f45aae0d3bc84639c791cad.md)

**郭文贵MILES** `2020-08-26T00:20:48`
##

[8月25日：刚刚接了个重要重要的电话．刚刚收到一个新的G时髦的瘦腿裤子．穿给曾经答应过的战友．穿瘦腿长裤給战友们看！](/content/getter/content/2020/08/5f458d21d3bc84639c78f6a2.md)

**郭文贵MILES** `2020-08-25T22:13:53`
##

[8月25日：我们的好战友．比尔盖茨．这篇文章．就是民主党坚定灭共的政治重锤呀！这在三年前是想都不敢想的大事呀……只有爆料革命在几年前就说这一天会发生……希望台湾...](/content/getter/content/2020/08/5f45674cd3bc84639c78d853.md)

**郭文贵MILES** `2020-08-25T19:32:28`
##

[8月25日：这篇重要的报导被转推的数字少得太可怜了，拜请战友们……我们大家一起．用你的金手指转起来！灭共没您不行，一定要转推．我们用生命和鲜血换来的．能证明我们...](/content/getter/content/2020/08/5f452b62d3bc84639c7882a5.md)

**郭文贵MILES** `2020-08-25T15:16:50`
##

[8月25号：尊敬的战友们好．请大家的焦点定在中共在美国实施的法律超限战。……，的事情，请大家关注华尔街日报，以及美国司法部，披露的中共在美国的法律超限战……以及...](/content/getter/content/2020/08/5f4515d5d3bc84639c78521f.md)

**郭文贵MILES** `2020-08-25T13:44:53`
##

[8月25日：祝尊敬的战友们．中国的七夕节快乐，祝天下人有情人．有义者……都能得到真心真意真爱，你们健身了吗？你们传播C C P病毒……香港危机真相了……一切都已...](/content/getter/content/2020/08/5f45017cd3bc84639c7827ae.md)

**郭文贵MILES** `2020-08-25T12:18:04`
##

[8月24日：乖乖呀，这是真的假的呀？](/content/getter/content/2020/08/5f43f34cb8c8a748917f5121.md)

**郭文贵MILES** `2020-08-24T17:05:16`
##

