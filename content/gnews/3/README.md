###  [:house:返回首頁](https://gitlab.com/leonalove/info)
---

## 大動作：美聯儲查封與朝鮮黑客劫持有關的加密貨幣賬戶
`2020-08-29 13:23 GM77` [轉載自GNews](https://gnews.org/zh-hant/323706/)

編撰：出埃及、文盲2020、文肯尼

美國司法部周四對280個加密貨幣賬戶提出民事沒收申訴，這些賬戶據稱被朝鮮黑客及其中共同夥利用，從十多家虛擬交易所竊取並進行洗錢，金額超過2.5億美元。

![](https://s3.amazonaws.com/gnews-media-offload/wp-content/uploads/2020/08/29131444/2-66.jpg)

朝鮮領導人金正恩在俄朝峰會上講話。來自GETTY 圖片

據《華爾街日報》報道，司法部和國內稅務局特工表示，美國官員通過數字化手段追蹤到朝鮮黑客，這些黑客利用惡意軟件進入交易所，從用戶賬戶中竊取資金，然後通過中共中間人洗錢。

今年3月，美國起訴並制裁了兩名中國公民，因為他們涉嫌幫助朝鮮黑客洗錢，被盜加密貨幣約1億美元。

“今天的行動公開揭露了朝鮮的網絡黑客項目與中共加密貨幣洗錢網絡之間的持續聯系，”司法部刑事司代理助理司法部長Brian Rabbitt說。

周四的申報是首個公開宣布的美國虛擬交易所被朝鮮盯上的案例。

根據聯合國專家和美國官員的說法，朝鮮利用網絡盜竊的收益來資助其軍事和核計劃。

**評語：**

天網恢恢，疏而不漏。朝鮮跟著中共狼狽為奸，沒想到在數字貨幣領域幹的非法勾當，都能被美國司法部盯上。正所謂魔高壹尺，道高壹丈。朝鮮盜竊數字貨幣就想搞武器、搞軍事恐嚇，根本不在乎是否提升本國民生，對朝鮮老百姓生死更是絲毫不感興趣。

根據郭先生視頻裏面提到的比特幣，近50%以上的比特幣被中共掌控。看似去中心化的比特幣，卻被中共集中化，在暗網用來向世界輸出CCP病毒，資助美國黑人Antifa擾亂美國社會秩序，假鈔交易，器官交易，暗殺，轉移盜國資產等等。

所幸，美國的科技實力具備監控數字貨幣，甚至摧毀邪惡數字貨幣的實力。根據郭先生視頻講解，目前IBM的量子計算機已經能正式應用，比特幣的區塊鏈算法，在IBM量子計算機面前，毫無價值，瞬間土崩瓦解！我們相信正義的力量都在等待最後致命壹擊，我們也奉勸跟極權政府同流合汙的走狗們，妳們幹的壞事我們都知道，而且我們有辦法解決！

新聞鏈接：
[https://www.forbes.com/sites/danielcassady/2020/08/27/feds-move-to-seize-cryptocurrency-accounts-linked-to-north-korean-hacker-heists/?utm\_source=newsletter&utm\_medium=email&utm\_campaign=dailydozen&cdlcid=5d0105821802c8c524bbf548#49751c0478a5](https://www.forbes.com/sites/danielcassady/2020/08/27/feds-move-to-seize-cryptocurrency-accounts-linked-to-north-korean-hacker-heists/?utm_source=newsletter&amp;utm_medium=email&amp;utm_campaign=dailydozen&amp;cdlcid=5d0105821802c8c524bbf548#49751c0478a5)

0
