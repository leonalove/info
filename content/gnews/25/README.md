###  [:house:返回首頁](https://gitlab.com/leonalove/info)
---

## 川普政府考慮將中共對維吾爾族穆斯林的壓迫標記為種族滅絕
`2020-08-29 07:44 喜馬拉雅戰鷹團 Himalaya Hawk Squad` [轉載自GNews](https://gnews.org/zh-hant/323193/)

*新聞來源：Foxnews《福克斯新聞》；作者：Paul Best；發佈時間：2020年8月26日*

*翻譯/簡評：Cathy r；校對：leftgun；審核：海闊天空；Page：拱卒*

簡評：

川普總統在今年6月簽署了“新疆人權法案”後，現在又提出要將中共對維吾爾人的壓迫定義為種族滅絕，川普總統一步一步加緊了對中共的控訴力度。對於這個本世紀最大的人權污點，川普總統沒有忘記，沒有掉以輕心，而是重點實錘真正落實制裁。可見川普政府絕不打口炮，而是步步為營，刀刀見血。

##  **川普政府考慮將中共對維吾爾族穆斯林的壓迫標記為種族滅絕** 

**中共國被指控在新疆大規模侵犯人權**
[!\[\](https://spark.adobe.com/page/075ddMx5a4TIq/images/668132d5-3e0b-4093-8172-250621653d98.jpg?asset_id=1d4ac036-3803-4820-9ab2-3482efd06909&amp;img_etag=%22a5cc3fbc236602c54794dfdb4f603f6a%22&amp;size=1024)](https://spark.adobe.com/page/075ddMx5a4TIq/images/668132d5-3e0b-4093-8172-250621653d98.jpg?asset_id=1d4ac036-3803-4820-9ab2-3482efd06909&amp;img_etag=%22a5cc3fbc236602c54794dfdb4f603f6a%22&amp;size=1024)
據《政治》（Politico）報導，川普政府正在考慮將中共國對新疆西部維吾爾穆斯林的壓迫貼上“種族滅絕”的標籤。

由于冠狀病毒大瘟疫、貿易分歧和香港自治，美國和中共國之間的緊張關係在2020年加劇。對種族滅絕的指控將進一步加劇這些緊張局勢。

白宮國家安全委員會（The White House National Security Council）星期二譴責中共國在新疆的行動。

[!\[\](https://spark.adobe.com/page/075ddMx5a4TIq/images/c8bbf8dc-55a8-42da-bd15-3641c4fe2f1b.jpg?asset_id=5c655951-a933-47ec-9ff4-014918b67414&amp;img_etag=%2281da4579c9aabceaaa95448f75752b9c%22&amp;size=1024)](https://spark.adobe.com/page/075ddMx5a4TIq/images/c8bbf8dc-55a8-42da-bd15-3641c4fe2f1b.jpg?asset_id=5c655951-a933-47ec-9ff4-014918b67414&amp;img_etag=%2281da4579c9aabceaaa95448f75752b9c%22&amp;size=1024)

“北京對維吾爾人的暴行包括對婦女的可怕行為，包括強迫墮胎、強迫絕育和其他強制節育方法、國家組織的強迫勞動和性暴力，包括在拘留期間強姦、漢族官員的強制住家和強迫婚姻。 ”美國國家安全委員會發言人約翰·烏利奧特（John Ullyot）在一份聲明中告訴福克斯新聞（Fox News）。 “中國共產黨的暴行還包括第二次世界大戰以來對少數民族的最大監禁。”

**中共國利用穆斯林維吾爾人強迫勞動力大規模生產冠狀病毒個人防護設備（PPE）出口全球。**

[!\[\](https://spark.adobe.com/page/075ddMx5a4TIq/images/f399c0ce-5a20-43ff-9366-055a6c7aaa47.jpg?asset_id=62624836-11b9-4026-964d-f6f2ba6ea436&amp;img_etag=%2233e0d63356d516c1860b3330aa39c853%22&amp;size=1024)](https://spark.adobe.com/page/075ddMx5a4TIq/images/f399c0ce-5a20-43ff-9366-055a6c7aaa47.jpg?asset_id=62624836-11b9-4026-964d-f6f2ba6ea436&amp;img_etag=%2233e0d63356d516c1860b3330aa39c853%22&amp;size=1024)建立在新疆的所謂教育營

阿德里安·澤茲(AdrianZenz)6月為詹姆斯敦基金(JamestownFoundation)提交的一份報告認為，2017年，北京開始了一場大規模絕育和強迫維吾爾人家庭分離的運動，以減少他們的自然人口增長。

澤茲寫道“這些調查結果令人嚴重關切的是，根據《聯合國防止及懲治滅絕種族罪公約》第二條D節的案文，北京在新疆的政策在根本上是否代表了可被稱為種族滅絕的人口運動。“

上個月，川普政府對據信應對新疆侵犯人權行為負責的多名中共國共產黨官員實施了製裁和簽證限制。國務卿邁克·彭佩奧（MikePompeo）說，這些侵犯人權行為“包括強迫勞動、任意大規模拘留和強迫人口控制，以及試圖抹殺他們的文化和穆斯林信仰。”

美國國務院週二沒有回應置評請求。

**報告說，中共國強迫穆斯林少數民族節育，墮胎以抑制人口數量**
[!\[\](https://spark.adobe.com/page/075ddMx5a4TIq/images/e0003651-2f61-4e41-9999-2fc8ec8e214f.jpg?asset_id=8f0ae353-4d24-49d3-b27d-9fd22b8b285a&amp;img_etag=%22ff6ce296f986b4b25360194ee262a956%22&amp;size=1024)](https://spark.adobe.com/page/075ddMx5a4TIq/images/e0003651-2f61-4e41-9999-2fc8ec8e214f.jpg?asset_id=8f0ae353-4d24-49d3-b27d-9fd22b8b285a&amp;img_etag=%22ff6ce296f986b4b25360194ee262a956%22&amp;size=1024)
國際組織也譴責北京對待維吾爾人的方式。聯合國反歧視委員會副主席蓋伊·麥克道格(Gay Mc Dougall)2018年表示，她的組織收到“大量可信的報告”，報告了新疆大規模侵犯人權的情況。

麥克道格在一次聽證會上說“據估計，100多萬人被關押在所謂的反極端主義中心，另有200萬人被迫進入所謂的政治和文化灌輸再教育營。”

中共國一再否認新疆發生了侵犯人權的事件。中共國駐華盛頓大使館週二沒有立即回應置評請求。

**作為遏制穆斯林人口運動的一部分，中共國被發現使用惡意軟件監視維吾爾人**
[!\[\](https://spark.adobe.com/page/075ddMx5a4TIq/images/b61e3871-9c2c-42b5-aabd-45ca1188c9c3.jpg?asset_id=08995c22-d530-446c-8efb-cafec7cfdd2b&amp;img_etag=%224f6e9989caedb7499e125eaf4eba67a7%22&amp;size=1024)](https://spark.adobe.com/page/075ddMx5a4TIq/images/b61e3871-9c2c-42b5-aabd-45ca1188c9c3.jpg?asset_id=08995c22-d530-446c-8efb-cafec7cfdd2b&amp;img_etag=%224f6e9989caedb7499e125eaf4eba67a7%22&amp;size=1024)
民主黨總統候選人拜登(JoeBiden)在競選過程中直言不諱地談到了中共國關押維吾爾穆斯林的問題。

拜登競選發言人安德魯·貝茨(AndrewBates)告訴《政治》說：“維吾爾族和其他少數民族在中共國獨裁政府手中遭受的難以言表的壓迫是種族滅絕，拜登以最強烈的立場反對。如果川普政府確實像喬·拜登(JoeBiden)所做的那樣，選擇對此呼籲，那麼迫切的問題是唐納德·川普(DonaldTrump)將採取什麼行動。他還必須為寬恕這種對維吾爾人可怕的待遇道歉。“

前川普國家安全顧問約翰·博爾頓(JohnBolton)在今年早些時候的回憶錄中聲稱，川普總統2019年鼓勵中共國總統習近平繼續建造關押維吾爾穆斯林的拘留營。

川普總統在接受《華爾街日報》(WallStreetJournal)採訪時駁斥了博爾頓的說法，稱他是“騙子”，並說“白宮的每個人都討厭約翰·博爾頓”。

川普總統還指出了他的政府對中共國的強硬立場，以及他於6月簽署了2020年”維吾爾人權政策法案“（UighurHumanRightsPolicyAct），該法案“對應對中共國新疆維吾爾自治區侵犯人權行為負責的外國個人和實體實施制裁，並要求就此專題提交各種報告” 。

[原文鏈接](https://www.foxnews.com/us/trump-administration-chinas-oppression-uighur-muslims-genocide-report)

編輯：【喜馬拉雅戰鷹團】

0
