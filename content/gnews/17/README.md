###  [:house:返回首頁](https://gitlab.com/leonalove/info)
---

## 臉書和谷歌重提海纜計劃 確定棄香港連接台、菲
`2020-08-29 09:19 GM01` [轉載自GNews](https://gnews.org/zh-hant/323341/)

國內新聞作者：文章

![](https://s3.amazonaws.com/gnews-media-offload/wp-content/uploads/2020/08/29090722/194.jpg)新聞來自：自由時報

據自由時報報道，臉書與Google已於當地時間27日修正了太平洋光纖網路（PLCN）海底電纜計劃，改連接美國與臺灣、菲律賓，而由中資主導的香港部份遭到排除。

美國司法部曾於6月中指出，原本的PLCN計劃中，負責香港部份的太平洋光纜數據公司（PLDC），其中國母公司——鵬博士電信傳媒集團與中國情報及安全部門有關系，擔憂中國藉此竊取美國用戶訊息。

原文鏈接：[https://ec.ltn.com.tw/article/breakingnews/3275452](https://ec.ltn.com.tw/article/breakingnews/3275452)

0
