###  [:house:返回首頁](https://gitlab.com/leonalove/info)
---

## 美國國家航空航天局資助的科學家被指控為中共國工作
`2020-08-29 07:43 喜馬拉雅戰鷹團 Himalaya Hawk Squad` [轉載自GNews](https://gnews.org/zh-hant/323216/)

*新聞來源：The Washington Times《華盛頓時報》；作者：Stephen Dinan /斯蒂芬·迪南；發佈時間：2020年8月24日*

*翻譯/簡評：風起雲湧；校對：Beicy-數學老師；審核：海闊天空；Page：拱卒*

**簡評：**

這是美國聯邦調查局查獲的又一起與’千人計劃’ 科學家有關的串謀、電信欺詐和盜竊信息案，相信還會有成百上千的中共間諜落入法網。這些被指控的科學家涉及的領域極其廣泛，很多美國人用幾十年，甚至上百年的積累才研製出來的新成果，一夜之間就被’千人計劃’ 偷回了中共國。對於知識產權偷竊，美國這次一定不會善罷甘休，一定會絕地反擊。不僅制裁這些個人，還會制裁背後指使他們行竊的中共。

自做聰明的中共，作為一個獨裁政權，沒有能力發展自己的尖端產品，只會用這些卑鄙的手段竊取暫時的利益，這不能換來持久的發展。他們在邪惡的道路上越走越遠，最終，也許就在不久的將來，一定會被正義的力量取而代之！

##  **華盛頓時報：美國國家航空航天局資助的科學家被指控為中共國工作** 
[!\[\](https://spark.adobe.com/page/ENyaXl3VK3qKR/images/081663d7-e68a-4eed-a573-1565eb936fbb.jpg?asset_id=36e2ac0e-6e99-4d4a-b5e1-b362e56e6c58&amp;img_etag=%2286e81149f0d547d07d08ccf10b47a287%22&amp;size=1024)](https://spark.adobe.com/page/ENyaXl3VK3qKR/images/081663d7-e68a-4eed-a573-1565eb936fbb.jpg?asset_id=36e2ac0e-6e99-4d4a-b5e1-b362e56e6c58&amp;img_etag=%2286e81149f0d547d07d08ccf10b47a287%22&amp;size=1024) NASA 美國航天航空局
美國聯邦檢察官週一宣布一名由美國航天航空局（NASA）資助的科學家因欺詐罪和共謀罪受到指控，FBI稱他在接受美國政府資助的同時也在擔任中共國的研究員。

美國聯邦調查局（FBI）說，程正東（音）隱瞞了他與中共國的合作關係，包括參加旨在非法輸送美國研究成果的中共國“千人計劃”。如果他與中共的合作事先被知曉的話，他是不會被允許參加美國航天航空局（NASA）的資助項目的。
[!\[\](https://spark.adobe.com/page/ENyaXl3VK3qKR/images/1bb00369-60bd-46dd-b57f-4534caba1c69.jpg?asset_id=61679b63-6ba6-4419-baaa-a751196f5621&amp;img_etag=%227bb43ca186a1e6f987c70a332dff0a20%22&amp;size=1024)](https://spark.adobe.com/page/ENyaXl3VK3qKR/images/1bb00369-60bd-46dd-b57f-4534caba1c69.jpg?asset_id=61679b63-6ba6-4419-baaa-a751196f5621&amp;img_etag=%227bb43ca186a1e6f987c70a332dff0a20%22&amp;size=1024)
因此，他在他與德克薩斯農工大學的關係上撒了謊，當時他正在德克薩斯農工大學工作，而該大學與美國航天航空局（NASA）簽有合同，政府這樣指控道。

國家安全助理總檢察長約翰·戴默斯（John C. Demers）表示：“我們再次目睹了未公開參與中共國政府人才計劃所帶來的刑事後果。”

提起訴訟的德克薩斯州南區美國檢察官賴安·帕特里克（Ryan K.Patrick）說：“中共國正在從世界各地“偷磚”來建設它的經濟和各種學術機構。”

美國聯邦調查局（FBI）特工本傑明·哈珀（Benjamin Harper）寫道，美國國家航空航天局（NASA）的研究給予了鄭先生訪問國際空間站資源的權限。

美國聯邦調查局（FBI）說， 他因此提高自己在中共國的“千人計劃”中的地位。

這是川普政府發起的一系列針對腳踏兩隻船的科學家的指控中最新的案例，這些被指控的科學家既在美國大學或公司工作，也為中共國工作，他們還將研究成果輸送給北京控制的利益集團。

聯邦法律已經禁止將美國國家航空航天局（NASA）的研究項目合同給予和中共國有關聯的實體或個人多年。

該法律特別包括了由中共中央教育部管轄的各類中共國大學在它的禁令中。聯邦調查局說，程先生曾是廣東科技大學的教授，之後，他還擔任了該大學一個研究所的所長。

鄭先生被指控犯有串謀、電信欺詐和陳述虛假信息等罪名。
[!\[\](https://spark.adobe.com/page/ENyaXl3VK3qKR/images/683b852d-1718-4329-9255-6bb534bcaf87.jpg?asset_id=dd45c8e1-0523-4276-afcc-9b090793ac24&amp;img_etag=%22921e5cfde8207fa9fe43cad6922b1497%22&amp;size=1024)](https://spark.adobe.com/page/ENyaXl3VK3qKR/images/683b852d-1718-4329-9255-6bb534bcaf87.jpg?asset_id=dd45c8e1-0523-4276-afcc-9b090793ac24&amp;img_etag=%22921e5cfde8207fa9fe43cad6922b1497%22&amp;size=1024)今天，一份刑事起訴書已經解封，指控53歲的德克薩斯州College Station的鄭正東犯有陰謀、做虛假陳述和電報詐騙罪。 (校園改革圖片) 
內布拉斯加州共和黨參議員本·薩斯（Ben Sasse）說：“這很明確：如果您為美國國家航空航天局（NASA）工作，您就不能為習主席工作。中國共產黨的戰略是用撒謊、欺騙和竊取的方式來達到科技上的統治地位，我們不能讓這種情況發生。 這次逮捕是個好消息，應該會警示其他人不要與北京過於曖昧。”

[原文鏈接](https://www.washingtontimes.com/news/2020/aug/24/nasa-funded-scientist-charged-also-working-china/?utm_source=Boomtrain&amp;utm_medium=manual&amp;utm_campaign=newsalert&amp;utm_content=newsalert&amp;utm_term=newsalert&amp;bt_ee=mEuF%2FuGDpydaf0iEAWASev0DTchPQSerOHxjp4hfzfA5JHB3UnUwbpyX2nPVDquK&amp;bt_ts=1598293433391)

編輯：【 [喜馬拉雅戰鷹團](https://spark.adobe.com/page/ENyaXl3VK3qKR/) 】

0
