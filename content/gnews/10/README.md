###  [:house:返回首頁](https://gitlab.com/leonalove/info)
---

## 中國公民被指控在FBI調查可能向中國轉移敏感軟體期間破壞硬碟
`2020-08-29 09:57 GM31` [轉載自GNews](https://gnews.org/zh-hant/323434/)

Department of Justice

Office of Public Affairs

FOR IMMEDIATE RELEASE  Friday, August 28, 2020

**美國司法部**

**公共事務辦公室**

**即時發佈  2020年8月28日 星期五**

**Chinese National Charged with Destroying Hard Drive During FBI Investigation into the Possible Transfer of Sensitive Software to China**

### **中國公民被指控在FBI調查可能向中國轉移敏感軟體期間破壞硬碟**

A Chinese national and researcher at the University of California, Los Angeles has been arrested on federal charges of destroying evidence to obstruct an FBI investigation after he was observed throwing a damaged hard drive into a dumpster outside his apartment, the Justice Department announced today.

美國司法部今天宣佈，一名中國公民、加州大學洛杉磯分校研究員因涉嫌破壞證據妨礙聯邦調查局(FBI)調查，已被聯邦政府逮捕。此前，有人觀察到他將一個損壞的硬碟扔進公寓外的垃圾箱。

Guan Lei, 29, of Alhambra, was arrested pursuant to a one-count criminal complaint unsealed this afternoon during his initial appearance in United States District Court.

阿罕布拉市29歲的關磊是依據今天下午在美國地區法院首次出庭時公佈的一項刑事指控而被逮捕的。

The criminal complaint alleges that Guan, who was in the U.S. on a J-1 non-immigrant visa, threw a damaged hard drive into a trash dumpster near his residence on July 25.  The FBI recovered the damaged hard drive after Guan was not allowed to board a flight to China and after Guan refused the FBI’s request to examine his computer.  The affidavit in support of the complaint notes that the internal hard drive “was irreparably damaged and that all previous data associated with the hard drive appears to have been removed deliberately and by force.”

刑事起訴書稱，7月25日，持J-1非移民簽證在美國的關某將一個損壞的硬碟扔進了他家附近的垃圾箱。在關某不允許登上飛往中國的航班及拒絕FBI檢查其電腦的要求後，FBI恢復了損壞的硬碟。支援該申訴的證詞指出，內部硬碟”遭到了不可挽回的損壞，之前所有與硬碟有關的資料似乎都被故意和強制刪除了。”

According to the complaint, Guan is being investigated for possibly transferring sensitive U.S. software or technical data to China’s National University of Defense Technology (NUDT) and falsely denying his association with the Chinese military – the People’s Liberation Army – in connection with his 2018 visa application and in interviews with federal law enforcement. Guan later admitted that he had participated in military training and wore military uniforms while at NUDT.  One of Guan’s NUDT faculty advisors in China was also a lieutenant general in the PLA who developed computers used by the PLA General Staff Department, the PLA General Armament Department, Air Force, military weather forecasts, and nuclear technology. NUDT is “suspected of procuring U.S.-origin items to develop supercomputers with nuclear explosive applications “and has been placed on the Department of Commerce’s Entity List for nuclear nonproliferation reasons, according to the affidavit.

起訴書稱，關某因可能將美國敏感軟體或技術資料轉移到中國國防科技大學(NUDT)，並在2018年的簽證申請和接受聯邦執法部門問詢時，拒不承認自己與中國軍隊–中國人民解放軍的關係而受到調查。關某後來承認，他在中國國防科技大學(NUDT)期間參加了軍事訓練，並穿著軍裝。關某在中國國防科技大學(NUDT)的教員顧問之一，也是解放軍的一名中將，他曾為解放軍總參謀部、解放軍總裝備部、空軍、軍事天氣預報和核技術開發計算器。根據宣誓書，中國國防科技大學(NUDT) “涉嫌採購源自美國的物品，以開發具有核爆炸應用的超級計算器”，並因核不擴散原因被列入商務部的實體名單。

In addition to destroying the hard drive, the complaint alleges that Guan concealed digital storage devices from investigators and falsely told federal officials that he had not had any contact with the Chinese consulate during his nearly two-year stay in the U.S.

除了銷毀硬碟外，起訴書還稱，關某向調查人員隱瞞了數字存放裝置，並對聯邦官員謊稱，他在美國逗留近兩年期間沒有與中國領事館有過任何聯繫。

During his initial appearance this afternoon, Guan was ordered detained by a United States Magistrate Judge, who scheduled an arraignment for Sept. 17, 2020.

在今天下午的初次出庭中，美國地方法官下令將關某拘留，並安排在2020年9月17日進行提審。

A criminal complaint contains allegations that a defendant has committed a crime. Every defendant is presumed innocent until and unless proven guilty beyond a reasonable doubt.

刑事訴訟包含了對被告人犯罪的指控。除了在排除合理懷疑的情況下被證明有罪之外，每名被告都被推定為無罪。

The felony offense of destruction of evidence carries a statutory maximum sentence of 20 years in federal prison.

銷毀證據的重罪最高可判處20年聯邦監禁。

This case is being investigated by the FBI, Homeland Security Investigations, and U.S. Customs and Border Protection. The U.S. Department of State’s Diplomatic Security Service has provided substantial assistance during the investigation.

此案正由聯邦調查局、國土安全調查局和美國海關與邊境保護局調查。美國國務院外交安全局在調查期間提供了大量協助。

This case is being prosecuted by Assistant U.S. Attorneys Will Rollins and George Pence of the Terrorism and Export Crimes Section.

這起案件由美國反恐和出口犯罪部門的助理檢察官威爾·羅林斯和喬治·彭斯起訴。

連結：[https://www.justice.gov/opa/pr/chinese-national-charged-destroying-hard-drive-during-fbi-investigation-possible-transfer](https://www.justice.gov/opa/pr/chinese-national-charged-destroying-hard-drive-during-fbi-investigation-possible-transfer)

*翻譯：【班仔】 校對：【小粉紅】 編輯：【GM31】*

*戰友之家玫瑰園小對出品*

1
