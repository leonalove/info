###  [:house:返回首頁](https://gitlab.com/leonalove/info)
---

## 8月29日財經快訊: 脫鉤已經開始, 銀行瑟瑟發抖
`2020-08-29 08:26 秘密翻译组` [轉載自GNews](https://gnews.org/zh-hant/323264/)

![](https://s3.amazonaws.com/gnews-media-offload/wp-content/uploads/2020/08/29081217/Picture8-3.png)圖片來源:VCG 
[1.新西蘭證券交易所網站已連續4天遭受網絡攻擊。 28日當天僅開市一小時後被迫關閉,到新西蘭當地時間下午1：00點重新開放。新西蘭政府已介入調查並提供技術支持。此前由於新西蘭中共病毒疫情控制較好，股市一直逆勢上揚。本月11日開始出現社區感染病例，股市有待進一步觀察。](https://www.nzherald.co.nz/business/news/article.cfm?c_id=3&amp;objectid=12360250)

[2.舉世震驚：印度已經開始與中國在原油貿易中脫鉤，印度國有煉油廠上週決定停止向中國貿易公司（包括中海油，中石化和中石油）發送原油進口招標書。印度限制了中國的投資，取消了中國公司競標印度政府採購合同的資格，並禁止了將近300種中國開發的移動應用程序服務。](https://cn.reuters.com/article/india-china-oil-refinery-0827-thur-idCNKBS25O01D)

[3.康寶萊公司股價暴跌，紐約檢察官提起刑事訴訟，股票暫停交易。隨後ZeroHedge推特發布：檢察官本週五對於這起涉及共謀違反《美國反海外腐敗法》的指控疑似與康寶萊達成起訴延期共識。從2007年到2016年，康寶萊為中國官員（包括與政府機構和媒體接觸的官員）提供了腐敗福利，以期擴大在該國業務。](https://www.zerohedge.com/markets/herbalife-crashes-stock-halted-after-criminal-charge-new-york-prosecutors)

[4.彭培奧國務卿說： “美國很傷心地了解到中國共產黨繼續欺負我們的英國朋友及企業領導人。匯豐銀行讓那些因為剝奪香港人民自由而應受到製裁的個人繼續保留賬戶，卻關閉了那些追求自由的人們的賬戶。”](https://twitter.com/SecPompeo/status/1298575099164086278)

[5.川普總統在共和黨競選大會上宣佈如果他當選，共和黨對中政策: 1)產業鏈全部遷出中國，帶回美國本土，並對中共不再有任何依賴！ 2)打造一個超級能源大國，並保持能源上獨立。 3)在5G競賽中獲勝，並在世界上建立最好的網絡系統和導彈防禦系統。](https://www.youtube.com/watch?v=RhL9iFkBaus&amp;t=3661s)

[6. 32項違規罰沒92，000萬，農行建行連收11張罰單。](https://money.163.com/20/0829/08/FL6EEHQN00259DLP.html)

By：【密翻組· 金融法律組】

0
